package hoAconseguira;

import java.util.Scanner;

/**
 * <h2>La Clase hoAconseguira, s'utiliza per calcular la suma de varies posicions d'un 
 * array amb dues variables i la resta de la �ltima posici� del array amb una variable. </h2>
 * 
 * @version 04-2021
 * @author Alba Maria Jim�nez �lvarez
 * @since 14-03-2021
 */
public class hoAconseguira {
	
	public static void main(String[] args) {
		// Aquest programa est� basat en descobrir si l'Alba aprovar� l'assignatura de Programaci� 
		// jugant amb unes variables que poden fer trontollar aquest aprovat sumant o restant punts
		// a aquestes notes.
		
		Scanner reader = new Scanner(System.in);
		int casos = 0;
		String nom = "";
		double[] array = new double[3];
		double nota = 0;
		int dani = 0;
		int isabel = 0;
		int bebe = 0;

		// introducci� dels casos a analitzar
		casos = reader.nextInt();

		// introducci� de les variables
		for (int k = 0; k < casos; k++) {
			reader.nextLine();
			nom = reader.nextLine();

			for (int j = 0; j < array.length; j++) {
				nota = reader.nextDouble();
				array[j] = nota;
			}

			dani = reader.nextInt();
			isabel = reader.nextInt();
			bebe = reader.nextInt();

			metode(nom, array, dani, isabel, bebe);
		}

	}
	/**
	 * Metode que reb el nom, les notes i les variables i diu si l'Alba aprova, si no aprova o 
	 * si el programa nomes es per l'Alba.
	 * 
	 * @param nom. El par�metre nom reb el nom de la persona que vol probar el programa.
	 * @param array. El par�metre array reb les notes de les UF's.
	 * @param dani. El par�metre dani reb la variable boleana que indica si suma o no 1 punt a totes les UF's.
	 * @param isabel. El par�metre isabel reb la variable boleana que indica si suma o no 0,5 punts a totes les UF's.
	 * @param bebe. El par�metre bebe reb la variable boleana que indica si resta o no 2 punts a la �ltima UF.
	 * 
	 */
	public static void metode(String nom, double array[], int dani, int isabel, int bebe) {
		int sum = 0;
		double resultat = 0;

		if (nom.equalsIgnoreCase("alba")) {
			if (dani == 1) {
				array = suma(array, 1);
			}

			if (isabel == 1) {
				array = suma(array, 0.5);
			}

			if (bebe == 1) {
				array[array.length - 1] = resta(array, 2);
			}
			for (int i = 0; i < array.length; i++) {
				if (array[i] >= 5) {
					sum++;
				}
			}
			if (sum == 3) {
				System.out.println("Els astres s�han alineat! HO HA ACONSEGUIT!");
			} else {
				System.out.println("Aix� ja es veia venir.. No ho ha aconseguit..");
			}
			for (int i = 0; i < array.length; i++) {
				System.out.println(array[i] + " ");
			}
			
		} else {
			System.out.println("Perdona, aquest programa nom�s es per l'Alba, adeu.");
		}

	}
	
	/**
	 * Metode que reb l'array i calcula la suma de les variables que sumen.
	 * 
	 * @param array. El par�metre array reb les notes de les UF's.
	 * @param num. El par�metre num reb la variable que suma puntuaci� a les UF's
	 * 
	 * @return array
	 */
	public static double [] suma(double array[], double num) {
		for (int i = 0; i < array.length; i++) {
			array[i] = array[i] + num;
		}
		return array;
	}

	/**
	 * Metode que reb la �ltima posici� de l'array i calcula la resta de la variable que resta.
	 * 
	 * @param array. El par�metre array, en aquest cas, nom�s reb la �ltima posici� del array.
	 * @param num. El par�metre num reb la variable que resta puntuaci� de la �ltima UF.
	 * 
	 * @return resultat
	 */
	public static double resta(double array[], double num) {
		double resultat = 0;
		resultat = array[array.length - 1] - num;

		return resultat;
	}

}
