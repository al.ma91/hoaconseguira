package hoAconseguira;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

class hoAconseguiraTest {
	
	final static double[] array =  {3.5, 5, 5.5};
	
	@Test
	public void testSuma() {
		double[] res = hoAconseguira.suma(array, 0.5);
		double[] esperado = {4, 5.5, 6};
		assertArrayEquals(esperado, res);
	}
	
	@Test
	public void testSuma1() {
		double[] res1 = hoAconseguira.suma(array, 1);
		double[] esperado1 = {4.5, 6, 6.5};
		assertArrayEquals(esperado1, res1);
	}
	
	@Test
	public void testResta() {
		double res2 = hoAconseguira.resta(array, 2);
		assertEquals(3.5, res2);
	}

}
